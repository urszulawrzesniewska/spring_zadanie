package pl.cm.calc.io;

import org.springframework.stereotype.Component;

@Component("simpleOutputWriter")
public class SimpleOutputWriter implements OutputWriter {

  @Override
  public void write(final String message) {
    System.out.println(message);
  }
}
