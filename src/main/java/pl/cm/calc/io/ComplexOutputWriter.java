package pl.cm.calc.io;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("complexOutputWriter")
@Primary
public class ComplexOutputWriter implements OutputWriter {

    @Override
    public void write(final String message) {
        System.out.println("*"+message);
    }
}
