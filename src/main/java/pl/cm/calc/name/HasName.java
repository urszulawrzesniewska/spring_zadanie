package pl.cm.calc.name;

public interface HasName {
  String getName();

}
