package pl.cm.calc;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
  public static void main(String[] args) {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring-configuration.xml");

    final Calculator calculator = context.getBean(Calculator.class);
    calculator.start();
  }
}
