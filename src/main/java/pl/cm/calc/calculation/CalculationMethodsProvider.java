package pl.cm.calc.calculation;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CalculationMethodsProvider {

  private final List<Calculation> calculations;

  @Autowired
  public CalculationMethodsProvider( List<Calculation> calculations) {
    this.calculations = calculations;
  }

  public List<Calculation> getAvailableCalculationMethods() {
    return calculations;
  }
}
