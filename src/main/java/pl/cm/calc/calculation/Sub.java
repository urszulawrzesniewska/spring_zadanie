package pl.cm.calc.calculation;

import org.springframework.stereotype.Component;
import pl.cm.calc.name.NameProvider;

@Component
public class Sub extends NameProvider implements Calculation {

  public Sub() {
    super("sub");
  }

  public long calculate(long first, long second) {
    return first - second;
  }

}
