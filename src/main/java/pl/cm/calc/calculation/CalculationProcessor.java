package pl.cm.calc.calculation;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CalculationProcessor {

  private final CalculationMethodsProvider calculationMethodsProvider;

  @Autowired
  public CalculationProcessor(final CalculationMethodsProvider calculationMethodsProvider) {
    this.calculationMethodsProvider = calculationMethodsProvider;
  }

  public Optional<Long> calculate(final String operation, final long leftOperand, final long rightOperand) {
    final List<Calculation> availableCalculationMethods = calculationMethodsProvider.getAvailableCalculationMethods();
    for (Calculation calculation : availableCalculationMethods) {
      if (calculation.getName().equals(operation)) {
        final long result = calculation.calculate(leftOperand, rightOperand);
        return of(result);
      }
    }
    return empty();
  }

  public Optional<Long> calculateStreams(final String operation, final long leftOperand, final long rightOperand) {
    final List<Calculation> availableCalculationMethods = calculationMethodsProvider.getAvailableCalculationMethods();

    return availableCalculationMethods.stream()
        .filter(calculation -> calculation.getName().equals(operation))
        .map(calculation -> calculation.calculate(leftOperand, rightOperand))
        .findFirst();
  }
}
