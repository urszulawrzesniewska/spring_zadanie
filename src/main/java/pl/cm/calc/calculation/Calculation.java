package pl.cm.calc.calculation;

import pl.cm.calc.name.HasName;

public interface Calculation extends HasName {
  long calculate(long first, long second);
}
