package pl.cm.calc.calculation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.cm.calc.name.NameProvider;

@Component
public class Div extends NameProvider implements Calculation {

    public Div() {
        super("div");
    }

    @Override
    public long calculate(long first, long second) {
        return first / second;
    }
}
