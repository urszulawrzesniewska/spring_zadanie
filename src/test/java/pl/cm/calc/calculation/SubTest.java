package pl.cm.calc.calculation;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class SubTest {

  private final Sub instance = new Sub();

  @Test
  @Parameters(value = { "1,2", "4,3" })
  public void should_sub(int leftOperand, int rightOperand) {
    final long result = instance.calculate(leftOperand, rightOperand);

    assertThat(result).isEqualTo(leftOperand - rightOperand);
  }

  @Test
  public void should_return_proper_name() {
    final String name = instance.getName();

    assertThat(name).isEqualTo("sub");
  }
}